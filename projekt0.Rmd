---
title: "Projekt0"
author: "Felix"
date: "2022-10-29"
output:
  html_document:
    md_keep: true
    theme: 
      bootswatch: spacelab
      higlight: tango
    toc: true
    toc_float: 
      collapsed: false
      smooth_scroll: false
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(sozoeko1)
```

# This is Headline 1

*is this italic?* **is this bold?**

[This is a Link](https://bookdown.org/yihui/rmarkdown/html-document.html)

## This is Headline 2

Is this a List?
* List
* Lister
* Listen

### This is Headline 3

# Resluts {.tabset}

## Plots text

text

## tabels

more text
